# EnecsysLogger #
  Written by Patrick Tingen
  Base script by duinsel, thanks for a great job!
  See forum http://gathering.tweakers.net/forum/list_messages/1627615 (dutch)
  
## Description: ##
  A perl script to log the output from your Enecsys inverters to pvoutput
  
----------------------------------------------------------------------

### What do you need: ###
  1. Perl
  2. Some perl modules, see installation
  3. An account on pvoutput.org, see below
  4. A config file, see below
 
### Installation: Perl ###
  Any Perl version should do, I used Strawberry Perl on Windows
  from http://strawberryperl.com/

### Installation: Perl Modules ###
*  Go to the prompt and type
*  cpan Math::BaseCnv
*  This should install all modules you need
  
### Installation: Account on pvoutput.org ###
*  Register yourself on http://http://pvoutput.org/
*  Go to the settings on http://http://pvoutput.org/account.jsp
*  In the settings, copy your api-key and system-id to a file on your pc
*  Set your api-key to 'enabled'
  
### Installation: Create config file ###
*  Create a file called enecsys.ini in the same dir as the logger script
*  Start with the info in enecsys-sample.ini and modify some lines
*  - debugfile  : set to filename for debugging, leave blank if no debugging wanted
*  - sleeptime  : time the script sleeps after succesful post to pvoutput
*  - correction : correction for loss of energy during transport from panel to grid,
*                 normally loss is ~1% so set to 0.99 or set to 0 if you don't want to use it
*  - apikey     : set your own api key from PVOutput. Remember to activate it
*  - systemid   : set your own system id from PVOutput
*  - interval   : time between posts to pvoutput. Defaults to 300 sec (5 min)
*  - address    : the ip address of your Enecsys gateway, look at its screen
*  - inverter N : for each of your inverters, add a line, keep numbering with no holes in the
*                 numbering. If you have 10 inverters, start with inverter1 and end with inverter10
  
### Run the script ###
*  Go to the folder where you placed the script and start with:
*  perl enecsysLogger.pl
