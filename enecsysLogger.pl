#!/usr/bin/perl
# 
# EnecsysLogger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Original script by Martijn van Duijn
# Adapted by Patrick Tingen
#
use Math::BaseCnv;
use Math::BaseCnv dig; # enable the dig option
use XML::Simple;
use LWP::Simple;
use DateTime;
use LWP::UserAgent;
use POSIX qw(strftime);
use Config::Simple('-lc'); # -lc means ignore case

my $UserAgent = new LWP::UserAgent;
my $NumReported = 0;

dig('url'); # select the right alphabet for the base64

# Read config file
# Config file should be named enecsys.ini and look like this:
#
# [general]
# debugfile  = debugging.txt
# sleeptime  = 10
# correction = 0.99
#
# [pvoutput]
# apikey    = 4d3e5xxxxxfd38364cxxxxx379fcd28fxxxxxxxx
# systemid  = 12345
# interval  = 300
# 
# [gateway]
# address   = 192.168.1.100
# inverter1 = 110094998
# inverter2 = 110095188
# inverter3 = 110106981
#
# ... as much inverters as you have, keep numbering
#
$Config = new Config::Simple('enecsys.ini');

# get info from config file
$DebugFile  = $Config->param("general.debugfile");
$SleepTime  = $Config->param("general.sleeptime");
$Correction = $Config->param("general.correction");
$Interval   = $Config->param("pvoutput.interval");
$Apikey     = $Config->param("pvoutput.apikey");
$SystemId   = $Config->param("pvoutput.systemid");
$Gateway    = $Config->param("gateway.address");

# Check values
if (!$Interval)   { $Interval   = 300; } # minimal nr of seconds between outputs to pvoutput
if (!$SleepTime)  { $SleepTime  = 10;  } # nr of seconds to sleep after succesful output
if (!$Correction) { $Correction = 1.0; } # correction for energy loss during transport

writeDebug("Startup Enecsys logger \n");
writeDebug("Apikey     = $Apikey   ");
writeDebug("SystemId   = $SystemId ");
writeDebug("Gateway    = $Gateway  ");

# loop to get all inverters from ini file
do
{
	$NumInv = $NumInv + 1;
	$ThisInv = $Config->param("gateway.inverter" . $NumInv);
	@InverterId[$NumInv] = $ThisInv; 
}while ($ThisInv != "");
$NumInv = $NumInv - 1;

# Report them in the log
for ($a = 1; $a <= $NumInv ; $a = $a + 1){
	writeDebug("Inverter $a = @InverterId[$a]");  
}

# Print welcome message to screen
print "Enecsys Logging Script \n";
print "---------------------- \n";
print " - Waiting for $NumInv inverters to report data, please wait...\n";

for (;;)  # Infinite loop to keep polling
{
	# remember date to detect a new day
	$Today = DateTime->now(time_zone => 'Europe/Amsterdam' )->ymd('');

	# start a new output file each day	
	if ($LogFile ne "enecsys-" . $Today . ".txt")
	{
		# set outputfile to file with current date
		$LogFile = "enecsys-" . $Today . ".txt" ; 
		writeDebug("Output to $LogFile \n");    

		#Write headers for the columns
		open WRITEFILE, ">>", $LogFile or die $!; # define outputfile
		print WRITEFILE "Time", "\t", "Energy", "\t", "Power", "\n";
		close WRITEFILE;
	}

	my $Parser  = new XML::Simple;
	my $Url     = "http://$Gateway/ajax.xml"; 
	my $Content = get $Url or die "Unable to get $Url\n";
	my $Data    = $Parser->XMLin($Content);
	my $Now     = DateTime->now(time_zone => 'Europe/Amsterdam' ); #adjust timezone if desired. If unspecified time is UTC.

	$Zigbee = $Data->{zigbeeData}; #pick out the zigbee field
	$Zigbee =~ s/\r//g; #Remove lifefeed and CR from the string
	$Zigbee =~ s/\n//g; #Usually chomp is used for that, but there are issues between platforms with that

	if ($Zigbee =~ /^WS/  && length($Zigbee)==57  ) #Normal inverter strings are parsed 
	{
		dig('url');
		$DecZigbee = cnv('A'.substr($Zigbee,3,54),64,10); #decimal representation of whole Zigbee string
		dig('HEX'); #the url alphabet messes up de dec hex conversions, so change to HEX
		$HexZigbee = cnv($DecZigbee,10,16); #Hex representation of zigbeestring
		if (length($HexZigbee) == 80) # if we have a leading 0 it gets chopped off, this is a fix for that.
		{
			$HexZigbee="0".$HexZigbee;
		}
		$DeviceIdHex  = substr($HexZigbee,0,8); #Device ID in hex
		$IDEndian     = unpack("H*", pack("V*", unpack("N*", pack("H*", $DeviceIdHex)))); # convert from little to big endian
		$DeviceIdDec  = cnv($IDEndian,16,10); #Device ID in decimal numbers. Should match Enecsys monitor site
		$Timestamp    = cnv(substr($HexZigbee,18,4),16,10);
		$CountDown    = cnv(substr($HexZigbee,30,6),16,10);
		$DCCurrent    = 0.025*cnv(substr($HexZigbee,46,4),16,10); #25 mA units?
		$DCPower      = cnv(substr($HexZigbee,50,4),16,10);
		$Efficiency   = 0.001*cnv(substr($HexZigbee,54,4),16,10); #expressed as fraction
		$ACFreq       = cnv(substr($HexZigbee,58,2),16,10);
		$ACVolt       = cnv(substr($HexZigbee,60,4),16,10);
		$Temperature  = cnv(substr($HexZigbee,64,2),16,10);
		$HexWh        = cnv(substr($HexZigbee,66,4),16,10);
		$HexkWh       = cnv(substr($HexZigbee,70,4),16,10);
		$LifekWh      = (0.001*$HexWh)+$HexkWh; # total generated in kWh
		$LifeTimeWh   = ($HexkWh*1000)+$HexWh; # total generated in Wh
		$ACpower      = $DCPower * $Efficiency;
		$DCVolt       = sprintf("%0.2f",$DCPower / $DCCurrent); 

		# 110055498 DC=0W 0V 0.05A AC=57V 50Hz 0W E=0 T=22C L=522.628kWh 
		writeDebug("$DeviceIdDec" . " DC=" . $DCPower . "W " . $DCVolt . "V " . $DCCurrent . "A "
			. "AC=".$ACpower . "W " . $ACVolt . "V " . $ACFreq . "Hz "
			. "E=" . $Efficiency . " T=" . $Temperature . "C L=" . $LifekWh . "kWh");
	}

	# Save total generated per inverter
	@LifeTimeWh[$DeviceIdDec] = $LifeTimeWh;

	# How many inverters have reported until now?
	# While we're at it, sum the totals
	$NumFound   = 0;
	$TotalWh    = 0;
	$TotalTemp  = 0;
	$TotalVolt  = 0;
	$TotalPower = 0;
	for ($a = 1; $a <= $NumInv ; $a = $a + 1)
	{
		if ( @LifeTimeWh[@InverterId[$a]] != 0) 
		{ 
			$NumFound   = $NumFound + 1;
			$TotalWh    = $TotalWh + @LifeTimeWh[@InverterId[$a]];
			$TotalTemp  = $TotalTemp + $Temperature;
			$TotalVolt  = $TotalVolt + $ACVolt;
			$TotalPower = $TotalPower + $ACpower;
		};
	}

	# If we have found an inverter, show message of progress
	if ($NumFound != $NumReported)
	{
		print " - $NumFound of $NumInv inverters seen so far.\n";
		$NumReported = $NumFound;
	}

	# Only export to PVOutput when all inverters are found 
	# and when the last output was at least X seconds ago
	if (($NumFound == $NumInv) && ((time - $LastOutput) > $Interval ))
	{
		# Remember last output
		$LastOutput = time;

		# Print header to screen once
		if (!$HeaderPrinted)
		{
			print "\nTime    ", "\t", "Energy", "\t", "Power", "\n";
			$HeaderPrinted = 1;
		}

		# Calc averages to output
		$Time       = $Now->hms(':'); 
		$Date       = $Now->mdy('-');
		$AvgTemp    = int( 10 * ($TotalTemp / $NumInv)) / 10; # trunc at 1 dec
		$AvgVolt    = int( 10 * ($TotalVolt / $NumInv)) / 10; # trunc at 1 dec
		$TotalKwh   = int($TotalWh / 100) / 10; # trunc at 1 dec

		# Do a small correction on the total kWh. There is a small loss in power when transporting
		# the energy from the panels to the grid. This loss seems to be about 1%. I subtract that
		# from the total so the numbers from pvoutput and my s0 pulse meter more or less match.
		$TotalKwh = $Correction * $TotalKwh;

		# Post to PVOutput
		my $UpdateUrl="http://pvoutput.org/service/r2/addstatus.jsp";
		my $UserAgent=LWP::UserAgent->new(default_headers=>HTTP::Headers->new("X-Pvoutput-Apikey"=>$Apikey,"X-Pvoutput-SystemId"=>$SystemId));
		$UserAgent->env_proxy;
		my @data=("d"=>strftime("%Y%m%d",localtime),
				"t"=>strftime("%H:%M",localtime),
				"c1"=>1,
				"v1"=>$TotalWh,
				"v2"=>$TotalPower,
				"v5"=>$AvgTemp,
				"v6"=>$AvgVol
				);

		my $res=$UserAgent->post($UpdateUrl,\@data) or die $!;
#		writeDebug("Push to pvoutput : ");
#		for ($a = 1; a$ <= $NumInv; a$ = a$ + 1)
#		{
#			my $ThisInverter = @InverterId[$a];
#			my $ThisEnergy   = @LifeTimeWh[@InverterId[$a]];
#			writeDebug("  - Inverter: $a  ID: $ThisInverter  kWh: $ThisEnergy");
#		}
		writeDebug("  - Total   : ".$TotalKwh."kWh");
		writeDebug("  - Response: ".$res->decoded_content."\n");

		# Write to log
		open WRITEFILE, ">>", $LogFile or die $!; 
		print WRITEFILE $Time, "\t", $TotalKwh, "\t", $TotalPower, "\n"; 
		close WRITEFILE;

		# Write to screen
		print $Time, "\t", $TotalKwh."kWh", "\t", $TotalPower."W", "\n";    
	}

	# No need to keep polling like a madman if we can only upload to PVOutput every X minutes
	sleep $SleepTime;
}

sub writeDebug{
	my ($DebugText) = @_;
	my $LogDate = strftime("%d-%m-%Y",localtime);

	if ($DebugFile ne "") 
	{
		open WRITEFILE, ">>", $DebugFile or die $!; 
		print WRITEFILE $LogDate,"\t",strftime("%H:%M:%S",localtime), "\t", $DebugText, "\n";
		close WRITEFILE;
	}
}